﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Linq;

namespace ChristmasTree
{
    class Program
    {
        public static int SizeX = 120, 
                          SizeY = 30;
        public static int UpdateTime = 500;
        public static int TreesCount = 5;
        public static char[,] Window;
        public static List<ChristmasTree> christmasTrees;
        public static Snow snow;

        static void Main(string[] args)
        {
            Console.SetWindowSize(SizeX, SizeY);
            Window = new char[SizeX, SizeY];
            christmasTrees = new List<ChristmasTree>();
            snow = new Snow(SizeX, SizeY);

            List<ChristmasTree> trees = new List<ChristmasTree>();
            for (int i = 0; i < TreesCount; i++)
                trees.Add(new ChristmasTree());

            christmasTrees = trees.OrderBy(x => x.Layer).ToList();

            Play();
        }

        public static void Play()
        {
            while (true)
            {
                Update();
                OnDraw();

                Thread.Sleep(UpdateTime);
            }
        }

        public static void Update()
        {
            snow.Move();
            snow.CreateSnowflake();
        }

        public static void OnDraw()
        {
            FillArray();

            foreach(ChristmasTree christmasTree in christmasTrees)
                christmasTree.Render(Window);

            snow.Render(Window);

            ConsoleApi.Print(ArrayToString(Window), (short)Window.GetLength(1), (short)Window.GetLength(0));
        }

        private static string ArrayToString(char[,] array)
        {
            string newString = "";

            for (int i = 0; i < array.GetLength(1); i++)
            {
                for (int j = 0; j < array.GetLength(0); j++)
                {
                    newString += array[j, i];
                }
            }

            return newString;
        }

        private static void FillArray()
        {
            for (int i = 0; i < Window.GetLength(1); i++)
            {
                for (int j = 0; j < Window.GetLength(0); j++)
                {
                    Window[j, i] = ' ';
                }
            }
        }
    }
}
