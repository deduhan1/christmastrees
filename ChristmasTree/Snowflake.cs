﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChristmasTree
{
    class Snowflake
    {
        public int PositionX { get; private set; }
        public int PositionY { get; private set; }
        public char Symbol { get; private set; }

        private static Random _random = new Random();

        public void Init(int positionX, int positionY, char symbol)
        {
            PositionX = positionX;
            PositionY = positionY;
            Symbol = symbol;
        }

        public bool TryChangePosition(int maxPositionY, int maxPositionX)
        {
            PositionY += 1;

            if (_random.Next(0, 2) == 0)
                PositionX += _random.Next(0, 1) == 0 ? -1 : 1;

            return (PositionX >= 0 && PositionX <= maxPositionX && PositionY >= 0 && PositionY <= maxPositionY) ? true : false;
        }
    }
}
