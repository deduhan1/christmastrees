﻿using System;
using System.Collections.Generic;

namespace ChristmasTree
{
    class Snow
    {
        private int _weight;
        private int _height;
        private List<Snowflake> _snowflakes;
        private Random _random;

        public Snow(int weight, int height)
        {
            _random = new Random();
            _snowflakes = new List<Snowflake>();
            _weight = weight;
            _height = height;

            for (int i = 0; i < height; i++)
            {
                CreateSnowflake();
            }
        }

        public void CreateSnowflake()
        {
            Snowflake snowflake = new Snowflake();
            snowflake.Init(_random.Next(0, _weight - 1), _random.Next(0, _height - 1), '*');
            _snowflakes.Add(snowflake);
        }
        public void CreateSnowflake(int positionY)
        {
            Snowflake snowflake = new Snowflake();
            snowflake.Init(_random.Next(0, _weight - 1), positionY, '*');
            _snowflakes.Add(snowflake);
        }


        public void Move()
        {
            for (int i = 0; i < _snowflakes.Count; i++)
            {
                if (!_snowflakes[i].TryChangePosition(_height - 1, _weight - 1))
                {
                    _snowflakes.RemoveAt(i);
                    CreateSnowflake(0);
                    i--;
                }
            }
        }

        public void Render(char[,] window)
        {
            foreach (Snowflake snowflake in _snowflakes)
            {
                window[snowflake.PositionX, snowflake.PositionY] = snowflake.Symbol;
            }
        }

    }
}
