﻿using System;
using System.Linq;

namespace ChristmasTree
{
    class ChristmasTree
    {
        private static Random _random = new Random();
        
        private int _positionX;
        private int _positionY;
        private int _height;
        private char _topSymbol = '8';
        private char _stemSymbol = '|';
        private char[,] christmasTree;

        public int Layer => _positionX;

        public ChristmasTree()
        {
            _height = _random.Next(14, 25);
            _positionX = _random.Next(10, Program.SizeX - 10);
            _positionY = _random.Next(0, Program.SizeY - 25);

            christmasTree = new char[(_height * 2 - 1), _height];

            FillArray();
            Create(_height);
        }

        public void Render(char[,] window)
        {
            for (int i = 0; i < christmasTree.GetLength(1); i++)
            {
                for (int j = 0; j < christmasTree.GetLength(0); j++)
                {
                    if (christmasTree[j, i] != '0')
                        window[_positionX + j, _positionY + i] = christmasTree[j, i];
                }
            }
        }

        private char[] StringParser(string str, char firstSymbol, char middleSymbol, char lastSymbol)
        {
            char[] symbols = str.ToArray();
            int first = 0, last = 0;

            for (int i = 0; i < symbols.Length; i++)
            {
                if (symbols[i] != '0')
                {
                    first = i;
                    break;
                }
            }

            last = symbols.Length - 1;
            if(first == last)
            {
                symbols[first] = '^';
                return symbols;
            }

            symbols[first] = firstSymbol;
            symbols[last] = lastSymbol;

            for (int i = first + 1; i < last; i++)
                symbols[i] = middleSymbol;

            return symbols;
        }

        private char[] StringParser(string str, char symbol)
        {
            char[] symbols = str.ToArray();

            for (int i = 0; i < symbols.Length; i++)
            {
                if (symbols[i] != '0')
                {
                    symbols[i] = symbol;
                }
            }

            return symbols;
        }

        private void Create(int height)
        {
            int stem = height / 4;
            int top = height - stem;

            for (int i = 0; i < top; i++)
            {
                string str = "";

                for (int j = 0; j < (top - i - 1); j++)
                    str += '0';

                for (int j = 0; j < (i * 2 + 1); j++)
                    str += _topSymbol;

                var array = top - 1 == i ? StringParser(str, '~') : StringParser(str, '/', '|', '\\');

                for(int j = 0; j < array.Length; j++)
                    christmasTree[j, i] = array[j];
            }

            for (int i = top; i < height; i++)
            {
                string str = "";

                for (int j = 0; j < 2 * (height / 3); j++)
                    str += '0';

                for (int j = 0; j < (height / 3); j++)
                    str += _stemSymbol;

                var array = height - 1 == i ? StringParser(str, '~') : StringParser(str, '{', '|', '}');

                for (int j = 0; j < array.Length; j++)
                    christmasTree[j, i] = array[j];
            }
        }

        private void FillArray()
        {
            for (int i = 0; i < christmasTree.GetLength(1); i++)
            {
                for (int j = 0; j < christmasTree.GetLength(0); j++)
                {
                    christmasTree[j, i] = '0';
                }
            }
        }
    }
}
